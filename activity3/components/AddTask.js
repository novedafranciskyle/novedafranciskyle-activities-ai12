import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

export default function AddTask({ submitHandler }) {
    const [text, setText] = useState('');

    const changeHandler = (val) => {
        setText(val);
    }

    return (
        <View>
            <TextInput
                style={styles.input}
                placeholder='Write a Task'
                onChangeText={changeHandler}
            />
            <Button onPress={() => submitHandler(text)} title='Add Task' color='#05595B'/>
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        marginBottom: 10,
        paddingHorizontal: 120,
        paddingVertical: 6,
        borderWidth: 2,
        borderRadius: 5,
    }
})