import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Home from './Home';
import Tasks from './Task';
import TabBar from './TabB';

const Tab = createBottomTabNavigator()
const TabNavigator = () => {
    return (
    <Tab.Navigator tabBar={(props) => <TabBar {...props} />}>
        <Tab.Screen
        name='Home'
        component={Home}
        initialParams={{ icon: 'home'}}
        />
        <Tab.Screen
        name='Tasks'
        component={Tasks}
        initialParams={{ icon: 'squared-plus'}}
        />
    </Tab.Navigator>
    );
};

export default TabNavigator;