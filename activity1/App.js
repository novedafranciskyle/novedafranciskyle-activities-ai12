import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput, Alert } from 'react-native';

export default function App() {
 const [text, setText] = useState("");

 const getValues = ()=> {
     Alert.alert("You just typed:", text);
 }
  return (
    <View style={styles.container}>
      <Text style={styles.text}>"Enter Text"</Text>
       <TextInput style={styles.input}
                onChangeText={(text) => setText(text)}
              />
      <Button onPress={() => getValues()} title="Tap" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input: {
          width: 200,
          borderWidth: 1,
          borderColor: 'yellow',
          borderRadius: 5,
        },
});
