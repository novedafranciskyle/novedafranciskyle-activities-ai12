import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { color } from "react-native/Libraries/Components/View/ReactNativeStyleAttributes";
import { StatusBar } from "react-native";

//Styles
const styles = StyleSheet.create({
  buttons: {
    paddingTop: 500,
    paddingLeft: 160,
  },
  context: {
    marginTop: 30,
    fontSize: 25,
    marginLeft: 10,
    marginRight: 10,
    color: "#E91E63",
    backgroundColor: "rgba(33, 33, 33, 0.9)",
    borderRadius: 15,
  },
  page: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#212121",
    fontStyle:"italic"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#544eec",
  },
  back: {
    height: 700,
    width: 370,
    justifyContent: "center",
  },
});
//Home Page
function HomeScreen({ navigation }) {
  return (
    <View style={styles.view}>
      <StatusBar backgroundColor="#544eec" />
      <ImageBackground
        source={require("./background/Home.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={50}
          color={"#544eec"}
          onPress={() => navigation.navigate("First")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//1st Page
function First({ navigation }) {
  return (
    <View style={styles.view}>
      <StatusBar backgroundColor="#544eec" />
      <ImageBackground
       source={require("./background/1st.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={50}
          color={"#544eec"}
          onPress={() => navigation.navigate("Second")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}
//Second Page
function Second({ navigation }) {
  return (
    <View style={styles.view}>
      <StatusBar backgroundColor="#544eec" />
      <ImageBackground
        source={require("./background/2nd.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={50}
          color={"#544eec"}
          onPress={() => navigation.navigate("Third")}
        ></Ionic>

      </ImageBackground>
    </View>
  );
}

//Third Page
function Third({ navigation }) {
  return (
    <View style={styles.view}>
      <StatusBar backgroundColor="#544eec" />
      <ImageBackground
        source={require("./background/3rd.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={50}
          color={"#544eec"}
          onPress={() => navigation.navigate("Fourth")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//Fourth Page
function Fourth({ navigation }) {
  return (
    <View style={styles.view}>
      <StatusBar backgroundColor="#544eec" />
      <ImageBackground
        source={require("./background/4th.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={50}
          color={"#544eec"}
          onPress={() => navigation.navigate("Fifth")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//Fifth Page
function Fifth({ navigation }) {
  return (
    <View style={styles.view}>
      <StatusBar backgroundColor="#544eec" />
      <ImageBackground
        source={require("./background/5th.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Ionic
          style={styles.buttons}
          name="ios-home-outline"
          size={50}
          color={"#544eec"}
          onPress={() => navigation.navigate("Home")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

const Stack = createNativeStackNavigator();
export default function App({}) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        
      >
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="First" component={First} />
        <Stack.Screen name="Second" component={Second} />
        <Stack.Screen name="Third" component={Third} />
        <Stack.Screen name="Fourth" component={Fourth} />
        <Stack.Screen name="Fifth" component={Fifth} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
